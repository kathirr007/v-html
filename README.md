# VHTML

![](https://img.shields.io/npm/v/@staszek998/v-html?color=%2314ffec)
![](https://img.shields.io/npm/l/@staszek998/v-html?color=%2314ffec)

> Simple _Vue_ component capable of rendering the passed-in HTML string **without** wrapping it within a redundant HTML tag.
>
> Why? Because `v-html` directive is not always an option 🙂

**Table of contents**

[[_TOC_]]

## Installation

### 1. Download the package

#### 1.A. Using NPM.

```bash
npm install --save @staszek998/v-html
```

#### 1.B. Using Yarn.

```bash
yarn add @staszek998/v-html
```

### 2. Implement the component within your _Vue_ application

_VHTML_ exposes two named exports: `Component` and `Plugin`. Here's how to use them:

#### 2.A. Using the `Component` export

##### 2.A.a. Installing the component globally, for use _within the whole app_

```typescript
// main.(j|t)s

import Vue from 'vue'
import { Component as VHTML } from 'v-html'

Vue.component('VHTML', VHTML)
```

##### 2.A.b. Installing the component locally, for use _within a single component_

```vue
<!-- MyComponent.vue -->

<script>
import { Component as VHTML } from 'v-html'

export default {
    components: { VHTML }
}
</script>
```

#### 2.B. Using the `Plugin` export

```typescript
// main.(j|t)s

import Vue from 'vue'
import { Plugin as VHTML } from 'v-html'

Vue.use(VHTML)
```

### 3. Update your project's configuration

_VHTML_ requires _Vue_ to be running with the **[runtime compiler](https://cli.vuejs.org/config/#runtimecompiler)
enabled**.

```javascript
// vue.config.js

module.exports = {
    runtimeCompiler: true
}
```

## Usage

You can utilise the _VHTML_ in two ways: using the `html` prop or by passing the HTML right into the component, using
the default `<slot>`.

### A. Using prop

```vue
<!-- MyComponent.vue -->

<template>
    <VHTML v-bind="{ html }"/>
    <!-- OR -->
    <VHTML :html="html"/>
</template>

<script>
export default {
    data () {
        return {
            html: '<h1>Hello, World!</h1>'
        }
    },
}
</script>
```

Expected output:

```html
<h1>Hello, World!</h1>
```

### B. Using slot

```vue
<!-- MyComponent.vue -->

<template>
    <VHTML>{{ html }}</VHTML>
</template>

<script>
export default {
    data () {
        return {
            html: '<h1>Hello, World!</h1>'
        }
    },
}
</script>
```

Expected output:

```html
<h1>Hello, World!</h1>
```

## Authors

- IDEALIGN Stanisław Gregor [📧](mailto:developer@staszek.codes) [🌐](https://gitlab.com/staszek.codes)

---

Copyright © 2021 IDEALIGN Stanisław Gregor

Licensed under the [MIT license](./LICENSE).
