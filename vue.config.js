/*******************************************************************************
 * @copyright 2021 IDEALIGN Stanisław Gregor
 ******************************************************************************/

module.exports = {
  lintOnSave: false,
  runtimeCompiler: true
}
