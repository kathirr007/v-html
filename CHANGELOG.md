# CHANGELOG

#### 1.0.6 | 2021.04.16

**FIXES:**

- `README.md` has been updated.

---

#### 1.0.5 | 2021.04.16

**IMPROVEMENTS:**

- `bugs` field in the `package.json` has been populated.

---

#### 1.0.4 | 2021.04.16

**IMPROVEMENTS:**

- `keywords` field in the `package.json` has been populated.

---

#### 1.0.3 | 2021.04.16

**FIXES:**

- Editor slip within the `README.md` has been fixed.

---

#### 1.0.2 | 2021.04.15

**IMPROVEMENTS:**

- Installation instructions have been updated (see `README.md`).

---

#### 1.0.1 | 2021.04.15

**FIXES:**

- The URLs of the image shields inside the `README.md` file have been updated to match the new package name.

---

## 1.0.0 | 2021.04.15

Initial release.
