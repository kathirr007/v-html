/*******************************************************************************
 * @copyright 2021 IDEALIGN Stanisław Gregor
 ******************************************************************************/

import Vue from 'vue'

import { Component as VHTML } from './index'

export const App = Vue.extend({
  name: 'App',
  components: { VHTML },

  data () {
    return {
      /**
       * The HTML string to render using the `<VHTML>` component.
       */
      html: `
        <main>
          <h3>Hello, from the <code>&lt;main&gt;</code>!</h3>
          <p>Donec id elit non mi porta gravida at eget metus. Maecenas sed diam eget risus varius blandit sit amet non magna. Curabitur blandit tempus porttitor. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Maecenas sed diam eget risus varius blandit sit amet non magna. Duis mollis, est non commodo luctus, nisi erat porttitor ligula, eget lacinia odio sem nec elit. Aenean lacinia bibendum nulla sed consectetur.</p>
        </main>
        <aside>
          <p>I'm the <code>&lt;aside&gt;</code> element, which means the <i>VHTML</i> can render multiple root elements!</p>
        </aside>
      `
    }
  },

  template: `
    <div id="app">
      <section>
        <h2>Using <code>props</code></h2>
        <VHTML v-bind="{ html }" />
      </section>
      <hr>
      <section>
        <h2>Using <code>slot</code></h2>
        <VHTML>{{ html }}</VHTML>
      </section>
    </div>
  `
})
