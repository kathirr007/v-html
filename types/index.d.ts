/*******************************************************************************
 * @copyright 2021 IDEALIGN Stanisław Gregor
 ******************************************************************************/
export { Plugin } from './plugin';
export { VHTML as Component } from './VHTML';
