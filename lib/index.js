"use strict";
/*******************************************************************************
 * @copyright 2021 IDEALIGN Stanisław Gregor
 ******************************************************************************/
Object.defineProperty(exports, "__esModule", { value: true });
exports.Component = exports.Plugin = void 0;
var plugin_1 = require("./plugin");
Object.defineProperty(exports, "Plugin", { enumerable: true, get: function () { return plugin_1.Plugin; } });
var VHTML_1 = require("./VHTML");
Object.defineProperty(exports, "Component", { enumerable: true, get: function () { return VHTML_1.VHTML; } });
//# sourceMappingURL=index.js.map